package com.ug.swingpresentation;

import javafx.stage.FileChooser;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Scanner;

public class DesignedForm extends JFrame implements ActionListener {
    private JTextField textFieldFilePath;
    private JButton buttonOpen;
    private JLabel labelFilePath;
    private JTabbedPane tabbedPaneMain;
    private JPanel panelTextOptions;
    private JCheckBox checkBoxIsReadOnly;
    private JPanel rootPanel;
    private JList listFileHistory;
    private JTextArea textAreaFileContents;
    private JPanel panelFileOpen;
    private JPanel panelEditTab;
    private JPanel panelHistoryTab;
    private JPanel panelRadioButtonGroup;
    private JSlider sliderFontSize;
    private JLabel labelFontSize;
    private JButton buttonPickFontColor;
    private JScrollPane scrollPaneFileHistory;
    private JScrollPane scrollPaneFileContents;
    private JButton buttonSave;

    JMenu fileMenu = new JMenu("File");
    JMenu helpMenu = new JMenu("Help");
    JMenuItem fileMenuItemExit = new JMenuItem("Exit");
    JMenuItem helpMenuItemAbout = new JMenuItem("About");

    private DefaultListModel<String> fileHistoryModel;

    DesignedForm() {

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setTitle("Java Swing Presentation");

        ImageIcon imageIcon = new ImageIcon("src/com/ug/swingpresentation/chappie.png");
        this.setIconImage(imageIcon.getImage());

        this.add(rootPanel);

        // construct menu
        fileMenu.add(fileMenuItemExit);
        helpMenu.add(helpMenuItemAbout);

        JMenuBar menuBar = new JMenuBar();

        menuBar.add(fileMenu);
        menuBar.add(helpMenu);

        this.setJMenuBar(menuBar);
        // end construct menu

        fileMenuItemExit.addActionListener(this);
        helpMenuItemAbout.addActionListener(this);

        buttonOpen.addActionListener(this);
        buttonSave.addActionListener(this);

        listFileHistory.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                readFileFromPath(listFileHistory.getSelectedValue().toString());
                textFieldFilePath.setText(listFileHistory.getSelectedValue().toString());
            }
        });

        checkBoxIsReadOnly.addActionListener(e -> textAreaFileContents.setEditable(!checkBoxIsReadOnly.isSelected()));

        buttonPickFontColor.addActionListener(this);

        sliderFontSize.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                Float newSize = (float) sliderFontSize.getValue();
                Font textFont = sliderFontSize.getFont().deriveFont(newSize);
                textAreaFileContents.setFont(textFont);
                textAreaFileContents.revalidate();
            }
        });

        fileHistoryModel = new DefaultListModel<>();
        listFileHistory.setModel(fileHistoryModel);

        this.pack();
        this.setVisible(true);
    }

    public void createUIComponents() {

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == fileMenuItemExit) {
            DesignedForm.this.dispose();
        }
        if(e.getSource() == helpMenuItemAbout) {
            JOptionPane.showMessageDialog(null,
                    "Created by Vladislav Dashtu for the UG Java Course, 2021",
                    "About", JOptionPane.INFORMATION_MESSAGE);
        }

        if(e.getSource() == buttonOpen) {
            if(textFieldFilePath.getText().isEmpty()){
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("C:\\Users\\Vladislav\\Desktop\\txt"));
                fileChooser.setAcceptAllFileFilterUsed(false);
                fileChooser.setFileFilter(new FileNameExtensionFilter("Text files (*.txt)", "txt"));

                int response = fileChooser.showOpenDialog(null);
                if(response == JFileChooser.APPROVE_OPTION) {
                    textFieldFilePath.setText(fileChooser.getSelectedFile().getAbsolutePath());
                }
                else {
                    return;
                }
            }

            readFileFromPath(textFieldFilePath.getText());

            if(!fileHistoryModel.contains(textFieldFilePath.getText())) {
                fileHistoryModel.addElement(textFieldFilePath.getText());
            }
        }

        if(e.getSource() == buttonSave) {
            if(textFieldFilePath.getText().isEmpty()){
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setCurrentDirectory(new File("C:\\Users\\Vladislav\\Desktop\\txt"));
                fileChooser.setAcceptAllFileFilterUsed(false);
                fileChooser.setFileFilter(new FileNameExtensionFilter("Text files (*.txt)", "txt"));

                int response = fileChooser.showSaveDialog(null);
                if(response == JFileChooser.APPROVE_OPTION) {
                    String targetPath = fileChooser.getSelectedFile().getAbsolutePath();
                    if(!targetPath.endsWith(".txt")) {
                        targetPath = targetPath + ".txt";
                    }
                    textFieldFilePath.setText(targetPath);
                }
                else {
                    return;
                }
            }

            saveFileToPath(textFieldFilePath.getText());
        }

        if(e.getSource() == buttonPickFontColor) {
            JColorChooser colorChooser = new JColorChooser();
            Color newColor = JColorChooser.showDialog(colorChooser, "Pick font color", textAreaFileContents.getForeground());
            if(newColor != null) {
                textAreaFileContents.setForeground(newColor);
            }
        }
    }

    private void saveFileToPath(String path) {
        File targetFile = new File(path);

        if(!targetFile.exists()) {
            try {
                targetFile.getParentFile().mkdirs();
                targetFile.createNewFile();
            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, "Can not create file at path: " + textFieldFilePath.getText(), "Error", JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
            }
        }

        try {
            FileWriter myWriter = new FileWriter(targetFile);
            myWriter.write(textAreaFileContents.getText());
            myWriter.close();
            JOptionPane.showMessageDialog(null, "File saved successfully!", "Success", JOptionPane.INFORMATION_MESSAGE);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Can not write to file at path: " + textFieldFilePath.getText(), "Error", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
        }
    }

    private void readFileFromPath(String path) {
        File theFile = new File(path);

        if(!theFile.exists()) {
            JOptionPane.showMessageDialog(null, "Could not find file at path: " + textFieldFilePath.getText(), "Error", JOptionPane.ERROR_MESSAGE);
            return;
        }

        try {
            Scanner myReader = new Scanner(theFile);
            textAreaFileContents.setText("");
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                textAreaFileContents.append(data+ System.lineSeparator());
            }
            myReader.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }

        this.pack();
    }
}
